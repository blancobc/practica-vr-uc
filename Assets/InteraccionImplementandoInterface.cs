﻿using UnityEngine;
using System.Collections;

public class InteraccionImplementandoInterface : MonoBehaviour, IGvrGazeResponder {

	// Use this for initialization
	void Start () {
		
	}
			
	// Update is called once per frame
	void Update () {
		
	}

	void LateUpdate() {
		GvrViewer.Instance.UpdateState();
	}
		

	#region IGvrGazeResponder implementation

	/// Called when the user is looking on a GameObject with this script,
	/// as long as it is set to an appropriate layer (see GvrGaze).
	public void OnGazeEnter() {

	}

	/// Called when the user stops looking on the GameObject, after OnGazeEnter
	/// was already called.
	public void OnGazeExit() {

	}

	/// Called when the viewer's trigger is used, between OnGazeEnter and OnPointerExit.
	public void OnGazeTrigger() {

	}

	#endregion


}
