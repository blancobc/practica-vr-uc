﻿using UnityEngine;
using System.Collections;

public class InteraccionPorTiempo : MonoBehaviour {

	public Material materialActivo, materialInactivo;
	public float limiteTiempo = 2f;
	private float tiempo;

	// Use this for initialization
	void Start () {
		
		tiempo = Mathf.Infinity;
		GetComponent<Renderer>().material = materialInactivo;
	}
		
	
	// Update is called once per frame
	void Update () {
		
		if(Time.time - tiempo > limiteTiempo){ 
			interaccion(); 
		} 
	}


	public void activa(bool activado){

		if(activado){
			GetComponent<Renderer>().material = materialActivo;
			Debug.Log("Activado");

			tiempo = Time.time;
		}

		else{
			GetComponent<Renderer>().material = materialInactivo;
			Debug.Log("Desactivado");	

			tiempo = Mathf.Infinity;
		}

	}


	public void interaccion(){
		Debug.Log("Interaccion");

		//generamos una posición aleatoria en una esfera envolvente de radio 1
		Vector3 nuevaPosicion = Random.onUnitSphere;
		//restringimos la altura
		nuevaPosicion.y = Random.Range(0.5f, 1f);
		//lo alejamos una distancia de nosotros
		float distancia = 2 * Random.value + 3f;
		//asignamos la posicion
		transform.localPosition = nuevaPosicion * distancia;
	}

}